# 初期スコア

```
./bin/benchmarker -t "localhost:8000" -u $PWD/userdata
{"pass":true,"score":5023,"success":4727,"fail":0,"messages":[]}
```

# TL

* とりあえずnginxを入れてリバースプロキシする
goのポートを変更

```
flag.Set("bind", ":8001")
```

* alpを入れてアクセスログを確認

```
curl -sLO https://github.com/tkuchiki/alp/releases/download/v0.2.4/alp_linux_amd64.zip
unzip alp_linux_amd64.zip
sudo mv alp /usr/local/bin/alp
```

nginxログをltsvに変更

```
log_format ltsv "time:$time_local"
                "\thost:$remote_addr"
                "\tforwardedfor:$http_x_forwarded_for"
                "\treq:$request"
                "\tstatus:$status"
                "\tmethod:$request_method"
                "\turi:$request_uri"
                "\tsize:$body_bytes_sent"
                "\treferer:$http_referer"
                "\tua:$http_user_agent"
                "\treqtime:$request_time"
                "\tcache:$upstream_http_x_cache"
                "\truntime:$upstream_http_x_runtime"
                "\tapptime:$upstream_response_time"
                "\tvhost:$host";
```

vhostのログ設定に

```
/etc/nginx/conf.d/virtual.conf
server {
    server_name _;
    listen 8000;
    proxy_set_header Host $http_host;

    access_log  /var/log/nginx/access_isu.log  ltsv;
    error_log  /var/log/nginx/error_isu.log;

    client_max_body_size 20M;

    location ~ .*\.(css|js|ico) {
        root /home/vagrant/private-isu/webapp/public;
        break;
    }

    location /img {
        root /home/vagrant/private-isu/webapp/public;
        break;
    }

    location / {
        proxy_pass http://127.0.0.1:8001;
    }
}
```

topを見ながらベンチ実行

```
./bin/benchmarker -t "localhost:8000" -u $PWD/userdata
{"pass":true,"score":5221,"success":4921,"fail":2,"messages":["ステータスコードが正しくありません: expected 422, got 413 (POST /)"]}
```

エラーが出たのでnginxのmax_client_sizeを指定

```
./bin/benchmarker -t "localhost:8000" -u $PWD/userdata
{"pass":true,"score":5526,"success":5173,"fail":0,"messages":[]}
```

スコアは特に変化なし


alpでアクセスログ解析

```
cat /var/log/nginx/access_isu.log | alp --cnt -r -q --aggregates "/posts/\d+","/image/\d+","/@\w+"
```

画像を返すのがたくさん

* スローログの分析

topを見るとmysqlのCPU負荷が高いのでスローログを調べてみる

pt-query-digestのインストール

```
wget percona.com/get/percona-toolkit.deb
sudo apt-get install libio-socket-ssl-perl libnet-ssleay-perl libterm-readkey-perl
sudo dpkg -i percona-toolkit.deb
dpkg -s percona-toolkit 
```

/etc/mysql/my.cnf を編集

```
slow_query_log_file = /var/log/mysql/mysql-slow.log
slow_query_log      = 1
long_query_time = 0
log_queries_not_using_indexes = 1
```

ログのアクセス権を付与

```
sudo chmod 775 -R /var/log/
```

リセット用のスクリプトをつくっておく

```
#!/bin/bash
set -ex

if [ -f /var/log/mysql/mysql-slow.log ]; then
    sudo mv /var/log/mysql/mysql-slow.log /var/log/mysql/mysql-slow.log.$(date "+%Y%m%d_%H%M%S")
fi
if [ -f /var/log/nginx/access_isu.log ]; then
    sudo mv /var/log/nginx/access_isu.log /var/log/nginx/access_isu.log.$(date "+%Y%m%d_%H%M%S")
fi
sudo systemctl restart mysql
sudo service memcached restart
sudo systemctl restart nginx
./app
```

ベンチ実行後にスローログ分析

```
pt-query-digest /var/log/mysql/mysql-slow.log
```

indexを張る

```
# index.sql
ALTER TABLE comments ADD INDEX idx1(post_id, created_at);
ALTER TABLE posts ADD INDEX idx1(created_at);
```

```
mysql -uroot isuconp < index.sql
```

indexを張ったことでmysqlのCPU負荷が300%くらいから80%に

アプリの負荷が170%くらいに

```
./bin/benchmarker -t "localhost:8000" -u $PWD/userdata
{"pass":true,"score":10458,"success":9813,"fail":0,"messages":[]}
```

再度pt-queryを実行したら画像を返すクエリが入っていたため表示がうまくできず

がんばってスクロール

別のindexを張る

```
ALTER TABLE comments ADD INDEX idx2(user_id);
ALTER TABLE posts ADD INDEX idx2(user_id);
```

再度ベンチ実行

```
./bin/benchmarker -t "localhost:8000" -u $PWD/userdata
{"pass":true,"score":10179,"success":9567,"fail":0,"messages":[]}
```

* DBに保存されている画像をpublic/image以下に書き出す

```
func getInitialize(w http.ResponseWriter, r *http.Request) {
        imageGenerate() // once
        dbInitialize()
        w.WriteHeader(http.StatusOK)
}

// 画像書き出し
func imageGenerate() {
        results := []Post{}

        rerr := db.Select(&results, "SELECT `id`, `mime`, `imgdata` FROM `posts` ORDER BY `id`")
        if rerr != nil {
                fmt.Println(rerr)
                return
        }

        for _, p := range results {
                saveImage(p.ID, p.Imgdata, imageExt(p.Mime))
        }
}

func saveImage(pid int, file []byte, ext string) {
        f, err := os.Create("../public/image/" + strconv.Itoa(pid) + ext)
        if err != nil {
                return
        }
        defer f.Close()
        io.Copy(f, bytes.NewReader(file))
}
```

go build -o app でビルド後に /initialize にアクセスし画像生成

終わったら imageGenerate() を消しておく

* 画像投稿時もファイルに書き出すように

```
func postIndex(w http.ResponseWriter, r *http.Request) {
        // ...

        pid, lerr := result.LastInsertId()
        if lerr != nil {
                fmt.Println(lerr.Error())
                return
        }

        saveImage(int(pid), filedata, ext)
}
```

* nginxで画像を返すように

```
    location ~ .*\.(css|js|ico|jpg|png|gif) {
        root /home/vagrant/webapp/public;
        break;
    }
```

mysqlのCPU負荷が60%に

アプリの負荷が170%くらい

```
./bin/benchmarker -t "localhost:8000" -u $PWD/userdata
{"pass":true,"score":16114,"success":15379,"fail":0,"messages":[]}
```

# 参考
* [Gojiを使ってWebビーコン作る - Qiita](http://qiita.com/sys_cat/items/1b8581de1344cc5db6bb)
* [日本語が化けるときの対処法。（Ubuntu/Debian版） - それマグで！](http://takuya-1st.hatenablog.jp/entry/20090801/1249116751)
* [GitHub - tkuchiki/alp: Access Log Profiler](https://github.com/tkuchiki/alp/tree/master)
* [ISUCON予選突破を支えたオペレーション技術 - ゆううきブログ](http://blog.yuuk.io/entry/web-operations-isucon)
* [UbuntuにPercona Toolkitインストール - Qiita](http://qiita.com/kei2100/items/7503a38ddee5c64ace97)
* [スローログの集計に便利な「pt-query-digest」を使ってみよう | Think IT（シンクイット）](https://thinkit.co.jp/article/9617)
* [Nginx での 413 Request Entity Too Large エラーの対処法 - Qiita](http://qiita.com/takecian/items/639deeae094466de6546)